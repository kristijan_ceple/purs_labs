/*****************************************************************************
* fir_integer.cpp
*****************************************************************************/
#include <cycle_count.h>
#include <stdio.h>
#include <ccblkfn.h>
#define Nsamp 10
#define L 21

#include "signal_integer.h"
#include "coeffs_integer.h"

short delay[L];
short idx_delay = 0;

short izlbuf[Nsamp];
short idx_izlbuf = 0;

void init_fir() {
	int i;
	
	// Ovdje sve treba postaviti na 0
	for(i = 0; i < L; i++) {
		delay[i] = 0;
	}
	
	// Sada postavit izlbuf
	for(i = 0; i < Nsamp; i++) {
		izlbuf[i] = 0;
	}
}

void obrada_integer(short sample)
{
	int i;
	int acc = 0;
	
	// zapamti najnoviji uzorak
	delay[idx_delay] = sample;	
	
	// cirkularno povecaj indeks delay niza (vidi napomenu)
	idx_delay = circindex(idx_delay, 1, L);
	//++idx_delay;
	//idx_delay %= L;
	
	// u petlji akumuliraj umnoske stanja i koeficijenata, uz cirkularno
	// inkrementiranje indeksa stanja pomocu circindex
	// voditi racuna o specificnostima fixed point aritmetike, odnosno
	// posmaku u lijevo za jedan nakon mnozenja da bi rezultat bio tocan
	// u 32 bitnoj frakcionalnoj aritmetici (jeste li proucili pripremu?)
	for(i = 0; i < L; i++) {
		// Sada tu ide acc - znaci mnozimo od pocetka koeficijente h i cirkularno samplese
		acc += (coeffs_integer[i]*delay[idx_delay]);
		
		// Nakon mnozenja moramo napraviti posmak za 1 ulijevo
		acc <<= 1;
		
		// Pomicanje indexa
		idx_delay = circindex(idx_delay, 1, L);
		//++idx_delay;
		//idx_delay %= L;
	}
	
	// zadrzi samo gornjih 16 bitova akumulatora (za sad ne brinemo oko
	// overflowa) i posmakni na najmanje znacajna dva bajta (>>16)
	acc >>= 16;
	izlbuf[idx_izlbuf++] = acc;
}

int main()
{
	int i;
	printf("Start of program!\n");
	
	init_fir();
	
	cycle_t start_count;
	cycle_t final_count;
	for(i=0;i<Nsamp;++i)
	{
		START_CYCLE_COUNT(start_count);
		obrada_integer(signal_integer[i]);
		STOP_CYCLE_COUNT(final_count,start_count);
		PRINT_CYCLES("Number of cycles: ", final_count);
		printf("Izlbuf[i]: %d\n", izlbuf[i]);
	}
	
	return 0;
}
