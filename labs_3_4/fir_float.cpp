/*****************************************************************************
* fir_float.cpp
*****************************************************************************/
#include <cycle_count.h>
#include <stdio.h>
#include <ccblkfn.h>
#define Nsamp 10
#define L 21
#include "signal_float.h"
#include "coeffs_float.h"

float delay[L];
short idx_delay = 0;

float izlbuf[Nsamp];
short idx_izlbuf = 0;

void init_fir() {
	int i;
	
	// Ovdje sve treba postaviti na 0
	for(i = 0; i < L; i++) {
		delay[i] = 0;
	}
	
	// Sada postavit izlbuf
	for(i = 0; i < Nsamp; i++) {
		izlbuf[i] = 0;
	}
}

void obrada_float(float sample)
{
	int i;
	float acc = 0;
	
	// zapamti najnoviji uzorak
	delay[idx_delay] = sample;	
	
	// cirkularno povecaj indeks delay niza (vidi napomenu)
	idx_delay = circindex(idx_delay, 1, L);
	//++idx_delay;
	//idx_delay %= L;
	
	// u petlji akumuliraj umnoske stanja i koeficijenata, uz cirkularno
	// inkrementiranje indeksa stanja (delay)
	for(i = 0; i < L; i++) {
		// Sada tu ide acc - znaci mnozimo od pocetka koeficijente h i cirkularno samplese
		acc += (coeffs_float[i]*delay[idx_delay]);
		idx_delay = circindex(idx_delay, 1, L);
		//++idx_delay;
		//idx_delay %= L;
	}
	
	izlbuf[idx_izlbuf++] = acc;
}

int main()
{
	int i;
	printf("Start of program!\n");
	
	init_fir();
	
	cycle_t start_count;
	cycle_t final_count;
	for(i=0;i<Nsamp;++i)
	{
		START_CYCLE_COUNT(start_count);
		obrada_float(signal_float[i]);
		STOP_CYCLE_COUNT(final_count,start_count);
		PRINT_CYCLES("Number of cycles: ", final_count);
		printf("Izlbuf[i]: %f\n", izlbuf[i]);
	}
	
	return 0;
}
