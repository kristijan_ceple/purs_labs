# Generated by the VisualDSP++ IDDE

# Note:  Any changes made to this Makefile will be lost the next time the
# matching project file is loaded into the IDDE.  If you wish to preserve
# changes, rename this file and run it externally to the IDDE.

# The syntax of this Makefile is such that GNU Make v3.77 or higher is
# required.

# The current working directory should be the directory in which this
# Makefile resides.

# Supported targets:
#     lab3_tut_Release
#     lab3_tut_Release_clean

# Define this variable if you wish to run this Makefile on a host
# other than the host that created it and VisualDSP++ may be installed
# in a different directory.

ADI_DSP=E:\Program Files (x86)\Analog Devices\VisualDSP 5.1.2


# $VDSP is a gmake-friendly version of ADI_DIR

empty:=
space:= $(empty) $(empty)
VDSP_INTERMEDIATE=$(subst \,/,$(ADI_DSP))
VDSP=$(subst $(space),\$(space),$(VDSP_INTERMEDIATE))

RM=cmd /C del /F /Q

#
# Begin "lab3_tut_Release" configuration
#

ifeq ($(MAKECMDGOALS),lab3_tut_Release)

lab3_tut_Release : ./Release/lab3_tut.dxe 

Release/fir_builtin.doj :fir_builtin.cpp $(VDSP)/Blackfin/include/fract.h $(VDSP)/Blackfin/include/fract_typedef.h $(VDSP)/Blackfin/include/fract_math.h $(VDSP)/Blackfin/include/ccblkfn.h $(VDSP)/Blackfin/include/stdlib.h $(VDSP)/Blackfin/include/yvals.h $(VDSP)/Blackfin/include/stdlib_bf.h $(VDSP)/Blackfin/include/builtins.h $(VDSP)/Blackfin/include/sys/builtins_support.h $(VDSP)/Blackfin/include/fr2x16_typedef.h $(VDSP)/Blackfin/include/r2x16_typedef.h $(VDSP)/Blackfin/include/raw_typedef.h $(VDSP)/Blackfin/include/sys/anomaly_macros_rtl.h $(VDSP)/Blackfin/include/sys/mc_typedef.h $(VDSP)/Blackfin/include/fr2x16_math.h $(VDSP)/Blackfin/include/fr2x16_base.h $(VDSP)/Blackfin/include/r2x16_base.h $(VDSP)/Blackfin/include/fract2float_conv.h $(VDSP)/Blackfin/include/filter.h $(VDSP)/Blackfin/include/complex.h $(VDSP)/Blackfin/include/complex_typedef.h signal_fract16.h $(VDSP)/Blackfin/include/cycle_count.h $(VDSP)/Blackfin/include/xcycle_count.h $(VDSP)/Blackfin/include/limits.h $(VDSP)/Blackfin/include/cycle_count_bf.h $(VDSP)/Blackfin/include/stdio.h 
	@echo ".\fir_builtin.cpp"
	$(VDSP)/ccblkfn.exe -c .\fir_builtin.cpp -c++ -file-attr ProjectName=lab3_tut -O -Ov100 -structs-do-not-overlap -no-multiline -D DO_CYCLE_COUNTS -double-size-32 -decls-strong -warn-protos -proc ADSP-BF537 -o .\Release\fir_builtin.doj -MM

Release/lab3_tut.doj :lab3_tut.cpp $(VDSP)/Blackfin/include/cplus/iostream $(VDSP)/Blackfin/include/cplus/istream $(VDSP)/Blackfin/include/cplus/ostream $(VDSP)/Blackfin/include/cplus/ios $(VDSP)/Blackfin/include/cplus/xlocnum $(VDSP)/Blackfin/include/cplus/climits $(VDSP)/Blackfin/include/yvals.h $(VDSP)/Blackfin/include/limits.h $(VDSP)/Blackfin/include/cplus/clocale $(VDSP)/Blackfin/include/locale.h $(VDSP)/Blackfin/include/cplus/cstdio $(VDSP)/Blackfin/include/stdio.h $(VDSP)/Blackfin/include/cplus/cstdlib $(VDSP)/Blackfin/include/stdlib.h $(VDSP)/Blackfin/include/stdlib_bf.h $(VDSP)/Blackfin/include/cplus/streambuf $(VDSP)/Blackfin/include/cplus/xiosbase $(VDSP)/Blackfin/include/cplus/cctype $(VDSP)/Blackfin/include/ctype.h $(VDSP)/Blackfin/include/cplus/stdexcept $(VDSP)/Blackfin/include/cplus/exception $(VDSP)/Blackfin/include/cplus/xstddef $(VDSP)/Blackfin/include/xsyslock.h $(VDSP)/Blackfin/include/sys/adi_rtl_sync.h $(VDSP)/Blackfin/include/stdbool.h $(VDSP)/Blackfin/include/stdint.h $(VDSP)/Blackfin/include/stddef.h $(VDSP)/Blackfin/include/cplus/cstddef $(VDSP)/Blackfin/include/cplus/xstring $(VDSP)/Blackfin/include/cplus/iosfwd $(VDSP)/Blackfin/include/cplus/cstring $(VDSP)/Blackfin/include/string.h $(VDSP)/Blackfin/include/cplus/cwchar $(VDSP)/Blackfin/include/wchar.h $(VDSP)/Blackfin/include/xwcstod.h $(VDSP)/Blackfin/include/xwstr.h $(VDSP)/Blackfin/include/cplus/xdebug 
	@echo ".\lab3_tut.cpp"
	$(VDSP)/ccblkfn.exe -c .\lab3_tut.cpp -c++ -file-attr ProjectName=lab3_tut -O -Ov100 -structs-do-not-overlap -no-multiline -D DO_CYCLE_COUNTS -double-size-32 -decls-strong -warn-protos -proc ADSP-BF537 -o .\Release\lab3_tut.doj -MM

./Release/lab3_tut.dxe :$(VDSP)/Blackfin/ldf/ADSP-BF537.ldf $(VDSP)/Blackfin/lib/bf534_rev_0.3/crtsf532y.doj ./Release/fir_builtin.doj ./Release/lab3_tut.doj $(VDSP)/Blackfin/lib/bf534_rev_0.3/__initsbsz532.doj $(VDSP)/Blackfin/lib/cplbtab537.doj $(VDSP)/Blackfin/lib/bf534_rev_0.3/crtn532y.doj $(VDSP)/Blackfin/lib/bf534_rev_0.3/libsmall532y.dlb $(VDSP)/Blackfin/lib/bf534_rev_0.3/libio532y.dlb $(VDSP)/Blackfin/lib/bf534_rev_0.3/libc532y.dlb $(VDSP)/Blackfin/lib/bf534_rev_0.3/librt_fileio532y.dlb $(VDSP)/Blackfin/lib/bf534_rev_0.3/libevent532y.dlb $(VDSP)/Blackfin/lib/bf534_rev_0.3/libcpp532y.dlb $(VDSP)/Blackfin/lib/bf534_rev_0.3/libf64ieee532y.dlb $(VDSP)/Blackfin/lib/bf534_rev_0.3/libdsp532y.dlb $(VDSP)/Blackfin/lib/bf534_rev_0.3/libsftflt532y.dlb $(VDSP)/Blackfin/lib/bf534_rev_0.3/libetsi532y.dlb $(VDSP)/Blackfin/lib/bf534_rev_0.3/libssl537y.dlb $(VDSP)/Blackfin/lib/bf534_rev_0.3/libdrv537y.dlb $(VDSP)/Blackfin/lib/bf534_rev_0.3/libusb537y.dlb $(VDSP)/Blackfin/lib/bf534_rev_0.3/libprofile532y.dlb 
	@echo "Linking..."
	$(VDSP)/ccblkfn.exe .\Release\fir_builtin.doj .\Release\lab3_tut.doj -flags-link -ip -L .\Release -flags-link -e -flags-link -od,.\Release -o .\Release\lab3_tut.dxe -proc ADSP-BF537 -MM

endif

ifeq ($(MAKECMDGOALS),lab3_tut_Release_clean)

lab3_tut_Release_clean:
	-$(RM) "Release\fir_builtin.doj"
	-$(RM) "Release\lab3_tut.doj"
	-$(RM) ".\Release\lab3_tut.dxe"
	-$(RM) ".\Release\*.ipa"
	-$(RM) ".\Release\*.opa"
	-$(RM) ".\Release\*.ti"
	-$(RM) ".\Release\*.pgi"
	-$(RM) ".\*.rbld"

endif


