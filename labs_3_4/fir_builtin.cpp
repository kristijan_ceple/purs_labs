/*****************************************************************************
* fir_builtin.cpp
*****************************************************************************/
#include <fract.h>
#include <filter.h>
#include "signal_fract16.h"
#include <cycle_count.h>
#include <stdio.h>
#include <ccblkfn.h>

#define L 21 /* broj uzoraka impulsnog odziva */
#define Nsamp 10


int main()
{
	int i;
	printf("Start of program!\n");
	
	cycle_t start_count;
	cycle_t final_count;
	
	fract16 izlbuf[Nsamp];
	section("L1_data_a") fract16 coeffs[L];
	section("L1_data_b") fract16 delay[L];
	fir_state_fr16 state;

	for(i = 0; i < L; i++) {
		delay[i] = 0;
	}
	
	fir_init(state, coeffs, delay, L, 0);
	fir_fr16(signal_fract16, izlbuf, Nsamp, &state);
	
	// Ispis rezultata
	for(i = 0; i < Nsamp; i++) {
		printf("Izlbuf[i]: %d\n", izlbuf[i]);
	}
		
	return 0;
}
