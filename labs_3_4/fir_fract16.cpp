/*****************************************************************************
* fir_fract16.cpp
*****************************************************************************/
#include <fract.h>
#include "signal_fract16.h"
#include "coeffs_fract16.h"
#include <cycle_count.h>
#include <stdio.h>
#include <ccblkfn.h>

#define L 21 /* broj uzoraka impulsnog odziva */
#define Nsamp 10

fract16 delay[L];
int idx_delay = 0;
fract16 izlbuf[Nsamp];
short idx_izlbuf = 0;

void init_fir() {
	int i;
	
	// Ovdje sve treba postaviti na 0
	for(i = 0; i < L; i++) {
		delay[i] = 0;
	}
	
	// Sada postavit izlbuf
	for(i = 0; i < Nsamp; i++) {
		izlbuf[i] = 0;
	}
}

void obrada_fract16(fract16 sample)
{
	int i;
	fract32 acc = 0;
	fract32 prod;
	
	// zapamti najnoviji uzorak
	delay[idx_delay] = sample;
	
	// cirkularno povecaj indeks delay niza (vidi napomenu)
	idx_delay = circindex(idx_delay, 1, L);
	
	// kao i u prethodnim zadacima implementirajte mnozenje i akumulaciju
	// ovaj put koristeci fract16 i fract32 tipove podataka.
	// da bi to napravili, koristite naredbe mult_fr1x32, add_fr1x32 i
	// round_fr1x32
	for(i = 0; i < L; i++) {
		// Prvo treba pomnoziti i-ti koeficijent sa cirkularnim history bufferom
		prod = mult_fr1x32(coeffs_fract16[i],delay[idx_delay]);
		
		// Nakon tog produkta to treba zbrojiti na acc
		acc = add_fr1x32(acc, prod);
		
		// Ne zaboraviti povecati cirkularni index cirkularnog history buffera
		idx_delay = circindex(idx_delay, 1, L);
	}
	
	// Fract32 treba roundati na fract16 koristeci biased rounding
	izlbuf[idx_izlbuf++] = round_fr1x32(acc);
}

int main()
{
	int i;
	printf("Start of program!\n");
	
	init_fir();
	
	cycle_t start_count;
	cycle_t final_count;
	
	for(i=0;i<Nsamp;++i)
	{
		START_CYCLE_COUNT(start_count);
		obrada_fract16(signal_fract16[i]);
		STOP_CYCLE_COUNT(final_count,start_count);
		printf("Number of cycles: %d\n",final_count);
		printf("%d\n", izlbuf[i]);
	}
	
	return 0;
}
