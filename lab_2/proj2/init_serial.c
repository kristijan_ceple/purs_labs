#include <at91sam7x512.h>
#define MCK 48000000 // fMCK=48MHz
#define BR 9600 // Zeljena brzina prijenosa
#define CD (MCK/16/BR) // konst. za generator takta za prijenos

void init_USART0 (void) {
	*AT91C_PMC_PCER=(1<<AT91C_ID_US0) | // Takt za USART0
		(1<<AT91C_ID_PIOA); // Takt za PIOA
	*AT91C_PIOA_PDR=AT91C_PA1_TXD0; // Dodjela prikljucaka
	*AT91C_US0_CR=AT91C_US_TXDIS | // Unemoguci odasiljac
	AT91C_US_RSTTX; // Resetiraj odasiljac
	*AT91C_US0_MR=AT91C_US_USMODE_NORMAL | // normalan nacin rada
		AT91C_US_CLKS_CLOCK | // takt za USART je MCK
	AT91C_US_CHRL_8_BITS | // znak ima 8 bitova
	AT91C_US_NBSTOP_1_BIT | // 1 stop bit
		AT91C_US_PAR_NONE; // nema pariteta
	// MSBF=0 => prvo ide LSB
	*AT91C_US0_BRGR=CD; // inic. generatora takta za prijenos
	*AT91C_US0_CR=AT91C_US_TXEN; // enable odasiljac
}
