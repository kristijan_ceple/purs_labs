			PRESERVE8
	
			AREA ASM_SWI_Handler, CODE, READONLY
			ARM


			EXPORT	ASM_SWI_Handler
			IMPORT	C_SWI_Handler

T_bit		EQU	0x20 ; Thumb bit (5) u CPSR/SPSR.
			STMDB	R13!,{R0-R12,R14} ; Pohrana registara na stog.

			MRS	R0, SPSR ; R2=SPSR_SVC

			TST	R0, #T_bit ; SWI je dosao iz Thumb stanja?
			; DA=>
			LDRNEH	R0, [R14,#-2] ; Ucitaj operacijski kod
			; SWI instrukcije (Halfword)
			BICNE	R0, R0, #0xFFFFFF00 ; Maskiraj "SWI number"

			; NE=>
			LDREQ	R0, [R14,#-4] ; Ucitaj operacijski kod
			; SWI instrukcije (Word)
			BICEQ	R0, R0, #0xFF000000 ; Maskiraj "SWI number"

			MOV	R1, R13 ; pokazivac na stog
			BL	C_SWI_Handler ; R0=SWI_number
			; R1=pokazivac na argumete

			LDMIA	R13!, {R0-R12,R15}^ ; Vracanje reg. sa stoga.
			END