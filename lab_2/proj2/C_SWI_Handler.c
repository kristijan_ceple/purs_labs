#include <string.h>
#include <stdio.h>
#define MORSE_ID 3
#define USART_ID 1

extern void sendchar_USART0 (int ch);
extern void send_char_morse (char ch);

void send_string (char *s) {
	while(*s != '\0') {
		sendchar_USART0(*s);
		s++;
	}
}

int SYS_OPEN(int *block){
	char *device_name = (char *) *block;
	//int opening_mode = *(block+1);
	//int name_length = *(block+2);
	if (!strncmp(device_name, ":tt",3)) {
		return USART_ID; // tty
	} else if(!strcmp(device_name, "morse")) {
		return MORSE_ID;
	} else { // ostalo nije implementirano
		return -1;
	}
}

int SYS_ISTTY(int *block){
	if (USART_ID == *block || MORSE_ID==*block) {
		return 1; // tty
	} else { // ostalo nije implementirano
		return 0;
	}
}

int SYS_WRITE(int *block){
	int handle = *block;
	char *str = (char *) (*(block+1));
	int string_size= *(block+2);
	
	if (USART_ID == handle || MORSE_ID == handle) { // tty
		while(string_size != 0) {
			if(USART_ID == handle) {
				sendchar_USART0(*str);
			} else if(MORSE_ID == handle) {
				send_char_morse(*str);
			}
			str++;
			string_size--;
		}
		return 0;
	} else { // ostalo nije implementirano
		send_string("SYS_WRITE : handle nije implementiran\n\r");
		return string_size;
	}
}

int SYS_WRITEC(int *block){
	sendchar_USART0(*block);
	return 0;
}

int SYS_DEF(int service_no){
	char buffer[128];
	sprintf(buffer, "\n\rUnimplemented SYS: 0x%X\n\r", service_no);
	send_string(buffer);
	return -1;
}

int SYS_CLOSE(int *block) {
	int handle = *block;
	if(handle == USART_ID || handle == MORSE_ID) {
		return 0;
	} else {
		send_string("SYS_CLOSE : handle not implemented\r\n");
		return -1;
	}
}

void PURS_agent (int *arguments) {
	int service_no, r, *block;
	service_no = *arguments; // R0 sa stoga
	block = (int *) (*(arguments+1)); // R1 sa stoga
	
	switch(service_no) {
		case (0x01) : r = SYS_OPEN(block); break;
		case (0x02) : r = SYS_CLOSE(block); break;
		case (0x03) : r = SYS_WRITEC(block); break;
		//case (0x04) : r = SYS_WRITE0(block); break;
		case (0x05) : r = SYS_WRITE(block); break;
		//case (0x06) : r = SYS_READ(block); break;
		//case (0x07) : r = SYS_READC(block); break;
		//case (0x08) : r = SYS_ISERROR(block); break;
		case (0x09) : r = SYS_ISTTY(block); break;
		//case (0x0A) : r = SYS_SEEK(block); break;
		//case (0x0C) : r = SYS_FLEN(block); break;
		//case (0x0D) : r = SYS_TMPNAM(block); break;
		//case (0x0E) : r = SYS_REMOVE(block); break;
		//case (0x0F) : r = SYS_REMOVE(block); break;
		//case (0x10) : r = SYS_CLOCK(block); break;
		//case (0x11) : r = SYS_TIME(block); break;
		//case (0x12) : r = SYS_SYSTEM(block); break;
		//case (0x13) : r = SYS_ERRNO(block); break;
		//case (0x15) : r = SYS_GET_CMDLINE(block); break;
		//case (0x16) : r = SYS_HEAPINFO(block); break;
		//case (0x17) :
		// r=angel_SWIreason_EnterSVC(block); break;
		//case (0x18) :
		// r=angel_SWIreason_ReportException(block); break;
		//case (0x30) : r = SYS_ELAPSED(block); break;
		//case (0x31) : r = SYS_TICKFREQ(block); break;
		// Nove arhitekture ovdje mozda budu imale jos poziva.
		// Zato default ...
		default : r = SYS_DEF(service_no); break;
	}
	*arguments = r;
}

void C_SWI_Handler(int SWI_number, int* arguments) {
	switch(SWI_number) {
		case(0x123456): PURS_agent(arguments); break;
		case(0xAB): PURS_agent(arguments); break;
		// Other cases go here
		default: send_string("\r\nUnknown SWI_number.\r\n");
	}
}
