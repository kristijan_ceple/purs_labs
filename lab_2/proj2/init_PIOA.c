#include <at91sam7x512.h>

#define ZVUCNIK_D102 AT91C_PIO_PA25

void init_PIOA(void) {

    // Ukljucivanje radnog takta za PIOA.
    *AT91C_PMC_PCER = (1 << AT91C_ID_PIOA);
    // pPMC -> PMC_PCER  = (1 << AT91C_ID_PIOA);
    
    // PIO kontroler upravlja prikljuckom PIOA25
    *AT91C_PIOA_PER = ZVUCNIK_D102;             

    // ukljucivanje izlaznog pojacala
    *AT91C_PIOA_OER = ZVUCNIK_D102;             

}
