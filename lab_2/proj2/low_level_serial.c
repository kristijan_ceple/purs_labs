#include <at91sam7x512.h>

void sendchar_USART0 (int ch) {
	while (!(*AT91C_US0_CSR & AT91C_US_TXRDY));
	// AT91C_US_TXRDY=(0x1<<1) => TXRDY Interrupt
	*AT91C_US0_THR = ch; // Transmitter Holding Register
}
