#include <at91sam7x512.h>

#define LED_H102 AT91C_PIO_PA21
#define LED_H101 AT91C_PIO_PA22

#define SLEEP_FOR_LOOP_CONSTANT 5999999
// 0.25 s
#define PIV_CONST 749999
// 4 * 0.25s = 1s
#define PICNT_CONST 4

unsigned char led_on = 0;
unsigned char piv_ov_cnt = 0;

// Get HW structs
AT91S_PMC* pmc_struct = AT91C_BASE_PMC;
AT91S_PITC* pit_struct = AT91C_BASE_PITC;
AT91S_PIO* pioa_struct = AT91C_BASE_PIOA;
AT91S_AIC* aic_struct = AT91C_BASE_AIC;
	
// Interrupt functions
void pit_handler(void) __irq;
void spurious_handler(void) __irq;

int main(void) {
	// Declarations
	unsigned int i;

	// Enable PIT
	pit_struct->PITC_PIMR = AT91C_PITC_PITEN | AT91C_PITC_PITIEN | PIV_CONST;
	
	// Peripheral clock enable
	pmc_struct->PMC_PCER |= LED_H102 | LED_H101;
	
	// Enable the peripheral itself
	pioa_struct->PIO_PER |= LED_H102 | LED_H101;
	
	// Set the peripheral as output
	pioa_struct->PIO_OER |= LED_H102 | LED_H101;
	
	/*
	 * Init AIC
	 */
	// Priority: 0, Edge: Rising(SRCTYPE = 3) in Source Mode Register(SMR)
	aic_struct->AIC_SMR[AT91C_ID_SYS] = (0x0) | (0x3<<5);
	
	// Set Source Vector Registers(SVR) for SYS and Spurious
	aic_struct->AIC_SVR[AT91C_ID_SYS] = (unsigned int)pit_handler;
	aic_struct->AIC_SPU = (unsigned int)spurious_handler;
	
	// Enable Interrupts in Interrupt Enable Control Register(IECR)
	aic_struct->AIC_IECR = 1<<AT91C_ID_SYS;
	
	// LED_H102 is managed by a while loop, while LED_H101 is managed by PIT+AIC
	while(1) {
		// Turn off LED(Clear Register)
		pioa_struct->PIO_CODR |= LED_H102;
		
		// Sleep
		for(i = 0; i < SLEEP_FOR_LOOP_CONSTANT; i++) {
			 //asm volatile ("":::"memory");
		}
	
		// Turn on LED(Set Register)
		pioa_struct->PIO_SODR |= LED_H102;
		
		for(i = 0; i < SLEEP_FOR_LOOP_CONSTANT; i++) {
			//asm volatile ("":::"memory");
		}
	}
	
	//return 0;
}

void pit_handler(void) __irq {
	// Check if PIT has issued an interrupt request
//	unsigned int current_picnt_val = pit_struct->PITC_PIIR & AT91C_PITC_PICNT;
//	current_picnt_val >>= 20;	
	if((*AT91C_PITC_PISR & AT91C_PITC_PITS)) {
		
		if(++piv_ov_cnt == PICNT_CONST) {
			// A minute has passed -> switch LED state
			led_on = (led_on+1)%2;
			piv_ov_cnt = 0;
			
			// Update LED state
			if(led_on) {
				pioa_struct->PIO_SODR |= LED_H101;
			} else {
				pioa_struct->PIO_CODR |= LED_H101;
			}
		}
		
		// Lower PITS by reading PIT_PIVR
		pit_struct->PITC_PIVR;
	}
	
	aic_struct->AIC_EOICR = 0;			// Tell AIC that the interrupt has been handled
}

/**
  * Dead loop function, should never happen either way because only edge interrupts are currently enabled.
	*/
void spurious_handler(void) __irq {
	aic_struct->AIC_EOICR = 0;			// Tell AIC that the interrupt has been handled
}
