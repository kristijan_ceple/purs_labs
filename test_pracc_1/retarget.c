#include <stdio.h>
#pragma import(__use_no_semihosting_swi)

extern void sendchar_USART0(int ch);

struct __FILE {
	int handle;
};

FILE __stdout;

int fputc(int ch, FILE *f) {
	sendchar_USART0(ch);
	return (ch);
}

int ferror(FILE *f) {
	return 0;
}

void _sys_exit(int return_code) {
	while(1);
}