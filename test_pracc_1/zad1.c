#include <stdio.h>
#include <at91sam7x512.h>

/*
 * Example midterm: Assignment 1
 * 1. Read PIOA28 state.
 *		a) If state == 1: configure bits 0-7 of PIOB as input pins with pull up resistor and glitch filter
 *		b) else: configure bits 0-7 of PIOB as output pins with two states and high Z state
 * Notes: PIOB yet needs to be configured. PIOA already is configured.
 */
int zad1(void) {
	unsigned int tmp;
	
	// First configure PIOB
	// For starters, let clock into PIOB
	*AT91C_PMC_PCER = (1<<3);
	
	// Time to read the pioa28 state and decide what to do next
	tmp = *AT91C_PIOA_PDSR & (1<<28);
	tmp >>= 28;
	
	*AT91C_PIOB_PER |= 0xFF;			// Peripheral Enable(in both cases)
	*AT91C_PIOB_PPUER |= 0xFF;		// Enable Pull-Up Resistor(in both cases - either for input or Multi-Driver)
	
	if(tmp == 1) {
		// Bits 0-7 need to be configured as input pins with Pull-Up Resistor and Glitch Filter
		*AT91C_PIOB_ODR |= 0xFF;			// Disable output --> enable input
		*AT91C_PIOB_IFER |= 0xFF;		// Enable input Glitch Filter
	} else {
		// Bits 0-7 need to be configured as output pins with two states and high Z state
		*AT91C_PIOB_OER |= 0xFF;			// Enable this pin as output
		*AT91C_PIOB_MDER |= 0xFF;		// Enable Multi-Driver
	}
	
	return 0;
}

void pit_handler(void) __irq;
void spurious_handler(void) __irq;

int zad2(void) {
	// For starters, turn on PIOB by letting clock in
	*AT91C_PMC_PCER = (1<<3);
	
	// Pins 0-7(output) and 8-15(input) should be used for Input-Output, and not for internal functions/devices
	*AT91C_PIOB_PER = 0xFFFF;		// Peripheral enable for pins 15-0
	*AT91C_PIOB_OER = 0xFF;			// Pins 0-7 are output
	*AT91C_PIOB_ODR = 0xFF00;		// Pins 0-7 are okay, but pins 8-15 are used for input
	*AT91C_PIOB_OWER = 0xFF;		// Let me write directly into those pins
	
	// Now configure PIT
	// No need to give it clock since SYSC is automatically clocked
	//*AT91C_PMC_PCER |= (1<<1);
	
	// Configure AIC now
	// For starters - interrupts have to be enabled in the processor core
	// Then we have to enable PIT interrupts in the AIC
	*AT91C_AIC_IECR |= 1<<1;
	// Priority = 0, Interrupt enabled on rising edge
	AT91C_AIC_SMR[1] = 0<<0 | 1<<5;
	// Finally put the location of the interrupt function
	AT91C_AIC_SVR[1] = (unsigned int)pit_handler;
	
	// Put location of the spurious interrupt handler
	*AT91C_AIC_SPU = (unsigned int)spurious_handler;
	
	// Let's configure PIT now
	*AT91C_PITC_PIMR = 1<<25 | 1<<24 | (299999);
	
	while(1) {
		// Infinite loop
	}
}

void pit_handler(void) __irq {
	// Check if PIT has requested interrupt
	if( (*AT91C_AIC_IPR & 0x1) && (*AT91C_PITC_PISR & 0x1) ) {
		// Read from input, put it into output
		unsigned int data_read = *AT91C_PIOB_PDSR & 0xFF00;
		data_read >>= 8;
		data_read &= 0xFF;
		
		// Now put it into the output pins
		*AT91C_PIOB_ODSR = data_read;
		
		// Reset by reading PIT PIVR
		*AT91C_PITC_PIVR;
	}
	
	// Tell AIC that interrupt has been processed
	*AT91C_AIC_EOICR = 1;
}

void spurious_handler(void) __irq {
	// Nothing
	// Tell AIC that interrupt has been processed
	*AT91C_AIC_EOICR = 1;
}

// Assignment 3:
int zad3(char input) {
	if(input != 'P') {
		return -1;
	}
	
	// No need to wait 4 MOSCS - it is already set
	// In Master Controller Register choose Slow Clock, and maximum Prescaler
	*AT91C_PMC_MCKR = 0b11100;
	
	// Wait until MCKRDY is not set
	while(!(*AT91C_PMC_SR & 1<<3));
	
	// Time to configure PLL
	*AT91C_CKGR_PLLR &= 0xFFFFFFF;				// Clear and prep the register for writing
	*AT91C_CKGR_PLLR |= 0x01<<0;					// Set DIV to 1
	*AT91C_CKGR_PLLR |= 0xFF<<8;					// Set PLLCOUNT to 0xFF
	*AT91C_CKGR_PLLR |= 0x00<<14;					// F_PLLCLK = 120 MHz, so set OUT to 00b -> 80-160 MHz
	*AT91C_CKGR_PLLR |= 0x005<<16;				// Set MUL  to 5
	
	// Now check/wait until PLL is ready
	while(!(*AT91C_PMC_SR & 1<<2));
	
	// When PLL is ready, need to switch PLLCLK -> MCK
	*AT91C_PMC_MCKR = 0b01011;
	
	// Wait until MCKRDY has been set
	while(!(*AT91C_PMC_SR & 1<<3));

	// OK, ready for action!

	return 0;
}

void init_USART0();
void sendchar_USART0(char ch);

int retargeting_pracc(void) {
	init_USART0();
	printf("Hello World!\n\r");
	
	while(1);
}

